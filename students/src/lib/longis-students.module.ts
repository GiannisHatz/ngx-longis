import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { Route, NavigationEnd,  Router, RouterModule, Routes } from '@angular/router';
import { el, en } from './i18n';
import { TranslateService } from '@ngx-translate/core';
import { APP_LOCATIONS, AppEventService } from '@universis/common';
import { STUDENTS_APP_LOCATIONS } from './app.locations';

function removeRoute(config: Routes, path: string): Route | undefined  {
  const commands = path.split('/');
  let index = 0;
  // get root
  let currentRoute = config.find((item) => item.path === '' && item.redirectTo == null);
  let currentRoutes: Routes = [];
  while (index < commands.length) {
    const loadedConfig: { routes: Routes } = (currentRoute as any)._loadedConfig;
    if (loadedConfig) {
      currentRoutes = loadedConfig.routes
    } else if (currentRoute && currentRoute.children) {
      currentRoutes = currentRoute.children
    }
    // try to get current route
    currentRoute = currentRoutes.find((item) => item.path === commands[index]);
    if (currentRoute == null) {
      return;
    }
    index += 1;
  }
  if (currentRoute) {
    const removeIndex = currentRoutes.findIndex((item) => item === currentRoute);
    if (removeIndex >= 0) {
      currentRoutes.splice(removeIndex, 1);
      return currentRoute;
    }
  }
}

@NgModule({
  imports: [
    CommonModule,
    RouterModule
  ],
  declarations: [],
  exports: []
})
export class LongisStudentsModule {
  constructor(private router: Router, private translate: TranslateService, private appEvent: AppEventService) {
    router.events.subscribe((routerEvent: any) => {
      if (routerEvent instanceof NavigationEnd) {
        const paths = [
          // 
        ];
        paths.forEach((path) => removeRoute(router.config, path));
      }
    });

    this.appEvent.add.subscribe((event?: { service: TranslateService | any, type: any }) => {
      if (event && event.service instanceof TranslateService && event.type === this.translate.setTranslation) {
        this.translate.setTranslation('el', el, true);
        this.translate.setTranslation('en', en, true);
      }
    });
  }

  static forRoot(): ModuleWithProviders<LongisStudentsModule> {
    return {
      ngModule: LongisStudentsModule,
      providers: [{
        provide: APP_LOCATIONS,
        useValue: STUDENTS_APP_LOCATIONS
      }]
    }
  }

}
