import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [],
  exports: []
})
export class LongisSharedModule {

  constructor() {
   //
  }

  static forRoot(): ModuleWithProviders {
    return {
      ngModule: LongisSharedModule,
      providers: []
    }
  }
}
